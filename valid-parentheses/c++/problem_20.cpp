/*
 * problem id: 20
 * link: https://leetcode.com/problems/valid-parentheses/
 */
#include <iostream>

using namespace std;

char getType(char ch) {
    char type; // "round brackets" () "square brackets" []  "curly brackets" {}
    switch (ch) {
        case '(':
        case ')':
            type = 'r';
            break;
        case '[':
        case ']':
            type = 's';
            break;
        case '{':
        case '}':
            type = 'c';
            break;
    }
    return type;
}

char getState(char ch) {
    return (ch == ')' or ch == ']' or ch == '}') ? 'c' : 'o';
}

class Solution {
public:
    bool isValid(string s) {
        short int len = s.length();
        bool res = true;
        if (len == 0) {
            return res;
        } else {
            res = false;
            if (len % 2 != 0 or s[0] == ')' or s[0] == ']' or s[0] == '}') {
                return res;
            } else {
                for (int i = 0; i < len; ++i) {
                    if (i + 1 < len) {
                        if (getState(s[i]) == 'o' and getState(s[i + 1]) == 'c') {
                            if (getType(s[i]) == getType(s[i + 1])) {
                                s.erase(i, 1);
                                s.erase(i, 1);
                                return isValid(s);
                            } else {
                                break;
                            }
                        } else if (getState(s[i]) == 'o' and getState(s[i + 1]) == 'o') {
                            continue;
                        } else {
                            break;
                        }
                    }
                }
                return res;
            }
        }
    }
};

int main() {
    Solution sol;
    cout << sol.isValid("(()]{}"); // 0
    cout << sol.isValid("()[]}"); // 0
    cout << sol.isValid("(){]");// 0
    cout << sol.isValid("{[]}"); // 1
    cout << sol.isValid("(([]){})"); // 1
    return 0;
}