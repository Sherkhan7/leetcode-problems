"""
problem id: 509
link: https://leetcode.com/problems/fibonacci-number/
"""


class Solution:
    def fib(self, n: int) -> int:
        if n in {0, 1}:
            return n
        return self.fib(n - 1) + self.fib(n - 2)


s = Solution()
# fib(10) = 55
print(s.fib(10))
print([s.fib(i) for i in range(11)])
