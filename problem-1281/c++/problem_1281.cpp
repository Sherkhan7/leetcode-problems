/*
 * problem id: 1281
 * link: https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/
 */
#include <iostream>

using namespace std;

class Solution {
public:
    int subtractProductAndSum(int n) {
        int p = 1, s = 0, r;
        while (n != 0) {
            r = n % 10;
            s += r;
            p *= r;
            n = n / 10;
        }
        return p - s;
    }
};

int main() {
    Solution sol;
    int n = 21233131;
    cout << sol.subtractProductAndSum(n);
    return 0;
}