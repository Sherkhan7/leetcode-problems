/*
 * problem id: 976
 * link: https://leetcode.com/problems/largest-perimeter-triangle/
 */
#include <iostream>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    int largestPerimeter(vector<int> &nums) {
        sort(nums.begin(), nums.end());
        for (int i = nums.size() - 1; i > 1; --i) {
            int a = nums.at(i - 2), b = nums.at(i - 1), c = nums.at(i);
            if (a + b <= c) {
                continue;
            }
            return a + b + c;
        }
        return 0;
    }
};

int main() {
    Solution sol;
    vector<int> nums{2, 2, 5};
//    vector<int> nums{3,2,3,4};
//    vector<int> nums{3, 2, 5, 4, 9};
//    vector<int> nums{5, 3, 3};
    cout << sol.largestPerimeter(nums);
    return 0;
}