from typing import List


class Solution:
    def largestPerimeter(self, nums: List[int]) -> int:
        nums = sorted(nums)
        i = len(nums)
        while i > 2:
            i -= 1
            a, b, c = nums[i - 2], nums[i - 1], nums[i]
            if a + b <= c:
                continue
            return a + b + c
        return 0


print(Solution().largestPerimeter([3, 2, 5, 4, 9]))
# print(Solution().largestPerimeter([1, 2, 1]))
