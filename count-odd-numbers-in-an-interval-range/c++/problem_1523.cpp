/*
 * problem id: 1523
 * link: https://leetcode.com/problems/count-odd-numbers-in-an-interval-range/
 */
#include <iostream>

using namespace std;

class Solution {
public:
    int countOdds(int low, int high) {
        int len = high - low + 1;
        if (len % 2 == 0) {
            return len / 2;
        } else if (low % 2 != 0) {
            return len / 2 + 1;
        } else {
            return len / 2;
        }
    }
};


int main() {
    Solution sol;
    cout << sol.countOdds(3, 7) << endl; // 3
    cout << sol.countOdds(2, 9) << endl; // 4
    cout << sol.countOdds(2, 12) << endl; // 5
    return 0;
}