from typing import List


class Solution:
    def canMakeArithmeticProgression(self, arr: List[int]) -> bool:
        if len(arr) > 2:
            arr = sorted(arr)
            d = arr[1] - arr[0]
            for i in range(2, len(arr)):
                if arr[i] - arr[i - 1] == d:
                    continue
                else:
                    return False
        return True


print(Solution().canMakeArithmeticProgression([3, 5, 1]))
print(Solution().canMakeArithmeticProgression([4, 2, 1]))
