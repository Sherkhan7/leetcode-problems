/*
 * problem id: 1502
 * link: https://leetcode.com/problems/can-make-arithmetic-progression-from-sequence/
 */
#include <iostream>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    bool canMakeArithmeticProgression(vector<int> &arr) {
        if (arr.size() > 2) {
            sort(arr.begin(), arr.end());
            int d = arr.at(1) - arr.at(0);
            for (int i = 2; i < arr.size(); ++i) {
                if (arr.at(i) - arr.at(i - 1) == d) { continue; }
                else { return false; }
            }
        }
        return true;
    }
};

int main() {
    Solution sol;
    vector<int> vect1{3, 5, 1};
    vector<int> vect2{1, 2, 4};
    cout << sol.canMakeArithmeticProgression(vect1);
    cout << sol.canMakeArithmeticProgression(vect2);
    return 0;
}