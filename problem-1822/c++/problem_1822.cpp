/*
 * problem id: 1822
 * link: https://leetcode.com/problems/sign-of-the-product-of-an-array/submissions/
 */
#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int arraySign(vector<int> &nums) {
        int s = 0;
        for (int i: nums) {
            if (i == 0) { return 0; }
            else if (i < 0) { s++; }
        }
        if (s % 2 != 0) { return -1; }
        else { return 1; }
    }
};

int main() {
    Solution sol;
    vector<int> nums{9, 72, 34, 29, -49, -22, -77, -17, -66, -75, -44, -30, -24};
//    vector<int> nums{3,2,3,4};
//    vector<int> nums{3, 2, 5, 4, 9};
//    vector<int> nums{5, 3, 3};
    cout << sol.arraySign(nums);
    return 0;
}