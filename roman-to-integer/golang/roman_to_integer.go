package main

import "fmt"

func romanToInt(s string) int {
	symbolValue := map[string]int{
		"I": 1,
		"V": 5,
		"X": 10,
		"L": 50,
		"C": 100,
		"D": 500,
		"M": 1000,
	}
	var answer int

	for i := len(s) - 1; i >= 0; i-- {
		iVal := symbolValue[string(s[i])]
		if i-1 >= 0 {
			leftChVal := symbolValue[string(s[i-1])]
			if iVal > leftChVal {
				answer += iVal - leftChVal
				i--
			} else {
				answer += iVal
			}
		} else {
			answer += iVal
		}
	}
	return answer
}

func main() {
	testNumbers := map[string]int{
		"MCDLXXVI":  1476,
		"MVIII":     1008,
		"DCCLXXXIX": 789,
		"MLXVI":     1066,
	}
	for romanSymbol, value := range testNumbers {
		answer := romanToInt(romanSymbol)
		fmt.Printf("Roman = %v, Value = %v, Answer = %v\n", romanSymbol, value, answer)
	}
}
