/*
 * problem id: 191
 * link: https://leetcode.com/problems/number-of-1-bits/
 */
#include <iostream>

using namespace std;

class Solution {
public:
    int hammingWeight(uint32_t n) {
        int s = 0;
        while (n != 0) {
            s++;
            n = n & (n - 1);
        }
        return s;
    }
};

int main() {
    Solution sol;
    int n = 60; //000000000000000000000000000111100
    cout << sol.hammingWeight(n) << endl;
    return 0;
}