class Solution:
    def hammingWeight(self, n: int) -> int:
        s = 0
        while n != 0:
            s += 1
            n = n & (n - 1)

        return s


m = 60  # 000000000000000000000000000111100
sol = Solution()
print(sol.hammingWeight(m))
