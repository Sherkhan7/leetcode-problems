/*
 * problem id: 1491
 * link: https://leetcode.com/problems/average-salary-excluding-the-minimum-and-maximum-salary/
 */
#include <iostream>
#include <vector>
#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    double average(vector<int> &salary) {
        int s = 0;
        sort(salary.begin(), salary.end());
        for (int i: salary) {
            s += i;
        }
        return (s - salary.front() - salary.back()) / double(salary.size() - 2);
    }
};

int main() {
    Solution sol;
    vector<int> vect{4000, 3000, 1000, 2000};
    cout << sol.average(vect) << endl;
    return 0;
}