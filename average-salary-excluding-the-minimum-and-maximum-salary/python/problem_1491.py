from typing import List


class Solution:
    def average(self, salary: List[int]) -> float:
        return (sum(sorted(salary)[1:-1])) / (len(salary) - 2)


sol = Solution()
print(sol.average([4000, 3000, 1000, 2000]))
